const express = require('express');
const router = express.Router();
const { registerUser, loginUser,getUserInfo,pathUserInfo,deleteUserInfo } = require('./usersService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/auth/register', registerUser);

router.post('/auth/login', loginUser);

router.get('/users/me', authMiddleware, getUserInfo);

router.patch('/users/me' , authMiddleware,pathUserInfo)

router.delete('/users/me', authMiddleware, deleteUserInfo)

module.exports = {
  usersRouter: router,
};
