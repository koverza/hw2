const express = require ('express')
const router = express.Router()
const {createNote,getNotes,getNote,putNote,patchNote,deleteNote} = require('./notesService.js')
const { authMiddleware } = require('./middleware/authMiddleware')

router.post('/',authMiddleware, createNote)

router.get('/',authMiddleware, getNotes)

router.get('/:id',authMiddleware,getNote)

router.put('/:id',authMiddleware,putNote)

router.patch('/:id',authMiddleware,patchNote)

router.delete('/:id',authMiddleware,deleteNote)

module.exports = {
    notesRouter: router,
};

