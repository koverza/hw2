const {User} = require('./models/Users.js');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {Note} = require("./models/Notes");

const registerUser = async (req, res, next) => {
    const {username, password} = req.body;

    const user = new User({
        username,
        password: await bcryptjs.hash(password, 10)
    });
    user.save()
        .then(() => res.json({message: 'Success!'}))
        .catch(err => {
            next(err);
        });
}

const loginUser = async (req, res, next) => {
    const user = await User.findOne({username: req.body.username});
    if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
        const payload = {username: user.username, userId: user._id};
        const jwtToken = jwt.sign(payload, 'secret-jwt-key');
        return res.json({
            message: "Success",
            jwt_token: jwtToken
        });
    }
    return res.status(400).json({'message': 'Not authorized'});
}

async function getUserInfo(req, res) {
    User.findById(req.user.userId)
        .then((user) => {
            res.status(200).send({
                "user": user
            })
        })
}

const deleteUserInfo = (req, res) => {
    User.findByIdAndDelete(req.user.userId)
        .then((user) => {
            res.status(200).send({
                "message": "Success"
            })
        })

}
async function pathUserInfo(req, res){
  const user = await User.findById(req.user.userId);
  if ((String(req.body.oldPassword), String(user.password))){
      user.password = req.body.newPassword
      user.password = await bcryptjs.hash(user.password, 10)
      user.save()
    res.status(200).send({
      "message": "Success"
    })
  }
  else {
    res.status(400).send({
      "message": "Error"
    })
  }
}

module.exports = {
    registerUser,
    loginUser,
    getUserInfo,
    deleteUserInfo,
    pathUserInfo
};
