const mongoose = require('mongoose');

const schema = mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate:{
    type: String
  }

},{timestamps:{updatedAt: false, createdAt: "createdDate"}});

let User = mongoose.model('User', schema)

module.exports = {
  User,
};
